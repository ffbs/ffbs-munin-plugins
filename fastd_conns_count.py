import socket
import sys
import os
import json
import yaml
from sets import Set

# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
sock_addr = sys.argv[1] #'/tmp/fastd_status.socket'
try:
	sock.connect(sock_addr)
except socket.error, msg:
	print >>sys.stderr, msg
	sys.exit(1)


try:
	connected = Set()
	data = "start" 
	all_data = ""
	while data:
		data = sock.recv(100)
		all_data +=data
	j = json.loads(all_data)
	noname_count = 0
	for peer in j["peers"]:
		p = j["peers"][peer]
		if  p["connection"]:
			try:
				connected.add(p["name"].encode("utf8"))
			except:
				noname_count += 1
				connected.add("noname"+str(noname_count))
	print "Connected nodes:"+str(len(connected))

finally:
	sock.close()
